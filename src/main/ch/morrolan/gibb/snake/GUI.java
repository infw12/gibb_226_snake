package main.ch.morrolan.gibb.snake;

import java.awt.Image;
import java.awt.Dimension;

import javax.swing.*;

public class GUI extends JFrame
{
    public JPanel panel;

    public GUI(int x, int y) {
        setTitle("Gibb 226 Snake");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        panel = new JPanel();
        setContentPane(panel);
        setContentSize(x, y);
    }

    public void setContentSize(int x, int y) {
        getContentPane().setPreferredSize(new Dimension(x, y));
        pack();
    }

    public void drawImage(Image image) {
        panel.getGraphics().drawImage(image, 0, 0, this);
    }
}
