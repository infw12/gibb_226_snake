package main.ch.morrolan.gibb.snake;

import java.awt.Point;
import java.awt.Color;
import java.util.HashMap;

/**
 * A class which represents a special body part of a snake. Namely, it contains two eyes.
 * Due to horrible genetic experiments, a snake is not limited to one head. There are even rumours that it does not require a head at all.
 */
public class Head extends BodyPart
{
    public HashMap<Direction, double[]> eye1Offset;
    public HashMap<Direction, double[]> eye2Offset;

    private double eyeRadius;

    /**
     * Create a new head, defaulting to a body part's default position.
     */
    public Head() {
        super();
        eyeRadius = 0.15;
        setUpEyes();
    }

    /**
     * Create a new head, using the supplied argument as its position.
     * @param point The initial position of the head.
     */
    public Head(Point point) {
        super(point);
        eyeRadius = 0.15;
        setUpEyes();
    }

    /**
     * Create a new head, using the two arguments as its initial position.
     * @param x The initial X coordinate of the head.
     * @param y The initial Y coordiante of the head.
     */
    public Head(int x, int y) {
        super(x, y);
        eyeRadius = 0.15;
        setUpEyes();
    }


    public double getEyeRadius() {
        return eyeRadius;
    }

    public void setEyeRadius(double eyeRadius) {
        this.eyeRadius = eyeRadius;
        setUpEyes();
    }



    private void setUpEyes() {
        double hEyeRadius = 0.5 * eyeRadius;

        eye1Offset = new HashMap<Direction, double[]>();
        eye1Offset.put(Direction.UP, new double[] {0.4 - hEyeRadius, 0.1 - hEyeRadius});
        eye1Offset.put(Direction.RIGHT, new double[] {0.9 - hEyeRadius, 0.6 - hEyeRadius});
        eye1Offset.put(Direction.DOWN, new double[] {0.6 - hEyeRadius, 0.9 - hEyeRadius});
        eye1Offset.put(Direction.LEFT, new double[] {0.1 - hEyeRadius, 0.4 - hEyeRadius});

        eye2Offset = new HashMap<Direction, double[]>();
        eye2Offset.put(Direction.UP, new double[] {0.6 - hEyeRadius, 0.1 - hEyeRadius});
        eye2Offset.put(Direction.RIGHT, new double[] {0.9 - hEyeRadius, 0.4 - hEyeRadius});
        eye2Offset.put(Direction.DOWN, new double[] {0.4 - hEyeRadius, 0.9 - hEyeRadius});
        eye2Offset.put(Direction.LEFT, new double[] {0.1 - hEyeRadius, 0.6 - hEyeRadius});
        
    }


    @Override
    public void draw(Painter painter) {
        // Todo: Test this function.
        // Todo: Add functionality to the painter class which allows to set the colour.
        super.draw(painter);

        painter.graphics.setColor(new Color(255, 0, 0));

        // Eye 1
        painter.drawOval(
                position.x + eye1Offset.get(direction)[0],
                position.y + eye1Offset.get(direction)[1],
                eyeRadius,
                eyeRadius
        );

        painter.drawOval(
                position.x + eye2Offset.get(direction)[0],
                position.y + eye2Offset.get(direction)[1],
                eyeRadius,
                eyeRadius
        );

        painter.graphics.setColor(new Color(0, 0, 0));
    }

}
