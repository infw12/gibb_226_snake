package main.ch.morrolan.gibb.snake;

import java.awt.Point;

import java.util.List;
import java.util.ArrayList;

/**
 * The main 'protagonist' in the game; A snake, consisting of several body parts. It eats diamonds, and dies if it collides with itself or a wall.
 * Initializing a new snake will have its name default to 'Eve', and its direction default to 'DOWN'
 */
public class Snake extends Entity
{
    // Todo: Tests for the setting of eyeRadius in the constructor, tests for setEyeRadius correctly updating the eye offset tables.
    // Todo: Maybe make the offset tables work with snakes which aren't one grid unit wide?
    // Todo: Tests for setting of frozen in constructor.
    public String name = "Eve";
    public Direction direction = Direction.DOWN;
    public List<BodyPart> body = new ArrayList<BodyPart>();
    public boolean frozen = false;

    /**
     * Create a new snake, consisting of one head with default position.
     */
    public Snake() {
        body.add(new Head());
    }

    /**
     * Create a new snake, consisting of one head with its position determined by the two parameters.
     * @param x X coordinate of the head's initial position.
     * @param y Y coordinate of the head's initial position.
     */
    public Snake(int x, int y) {
        body.add(new Head(x, y));
    }

    /**
     * Create a new snake, consisting of one head with its position determined by the supplied parameter.
     * @param point The head's initial position.
     */
    public Snake(Point point) {
        body.add(new Head(point));
    }


    /**
     * Move the whole snake (all of its parts) along the x axis.
     * @param i The number of tiles which to move the snake.
     */
    @Override
    public void moveX(int i) {
        for (BodyPart part : body) {
            part.moveX(i);
        }
    }

    /**
     * Move the whole snake (all of its parts) along the y axis.
     * @param i The number of tiles which to move the snake.
     */
    @Override
    public void moveY(int i) {
        for (BodyPart part : body) {
            part.moveY(i);
        }
    }

    /**
     * Move the whole snake (all of its parts) upwards (-y).
     * @param i The number of tiles which to move the snake.
     */
    @Override
    public void moveUp(int i) {
        for (BodyPart part : body) {
            part.moveUp(i);
        }
    }

    /**
     * Move the whole snake (all of its parts) downwards (+y).
     * @param i The number of tiles which to move the snake.
     */
    @Override
    public void moveDown(int i) {
        for (BodyPart part : body) {
            part.moveDown(i);
        }
    }

    /**
     * Move the whole snake (all of its parts) to the left (-x).
     * @param i The number of tiles which to move the snake.
     */
    @Override
    public void moveLeft(int i) {
        for (BodyPart part : body) {
            part.moveLeft(i);
        }
    }

    /**
     * Move the whole snake (all of its parts) to the right (+x).
     * @param i The number of tiles which to move the snake.
     */
    @Override
    public void moveRight(int i) {
        for (BodyPart part : body) {
            part.moveRight(i);
        }
    }


    /**
     * Make the snake draw all of its body parts.
     * @param painter An instance of the utility class which takes care of the transformation between grid tiles to pixels.
     */
    @Override
    public void draw(Painter painter) {
        for (BodyPart part : body) {
            part.draw(painter);
        }
    }

    /**
     * Make the snake perform logic operations.
     * @param tickCount The number of ticks which have passed since the start of the game.
     */
    @Override
    public void tick(long tickCount) {
        for (BodyPart part : body) {
            part.tick(tickCount);
        }
        if (tickCount % 5 == 0 && !frozen) {
            move(1);
        }
    }

    /**
     * Move each part of the snake according to the rules of the snake.
     * That is, each part moves into the direction of its previous neighbour. The first part moves in the direction of the snake object.
     * @param amount The number of tiles which to move the snake.
     */
    private void move(int amount) {
        Direction nextDirection;
        Direction previousDirection = direction;

        for (BodyPart part : body) {
            nextDirection = part.direction;
            part.direction = previousDirection;
            previousDirection = nextDirection;

            part.move(amount);
        }
    }


    /**
     * Add a body part to the snake. Body parts will be added at the end of the snake.
     */
    public void growBodyPart() {
        BodyPart newPart = new BodyPart();
        newPart.position = newPartPosition();
        body.add(newPart);
    }

    /**
     * Add a head to the snake. Heads will be added at the end of the snake.
     */
    public void growHead() {
        Head newHead = new Head();
        newHead.position = newPartPosition();
        body.add(newHead);
    }

    /**
     * Returns the position at which a new part (body, head) would/should spawn.
     * @return Position of a freshly spawned part.
     */
    private Point newPartPosition() {
        BodyPart lastPart = body.get(body.size() - 1);
        Point newPosition = new Point();

        switch (lastPart.direction) {
            case LEFT:
                newPosition.x = lastPart.position.x + 1;
                newPosition.y = lastPart.position.y;
                break;
            case RIGHT:
                newPosition.x = lastPart.position.x - 1;
                newPosition.y = lastPart.position.y;
                break;
            case UP:
                newPosition.x = lastPart.position.x;
                newPosition.y = lastPart.position.y + 1;
                break;
            case DOWN:
                newPosition.x = lastPart.position.x;
                newPosition.y = lastPart.position.y - 1;
                break;
        }

        return newPosition;
    }


    /**
     * @return A list of points, one for each body part's position.
     */
    public List<Point> positions() {
        List<Point> positions = new ArrayList<Point>();
        for (BodyPart part : body) {
            positions.add((Point)part.position.clone());
        }

        return positions;
    }

    // Todo: Tests for the two functions below.
    @Override
    public List<Point> collisionPoints() {
        return positions();
    }

    @Override
    public boolean collidesWith(Entity other) {
        List<Point> myCollisionPoints = collisionPoints();
        List<Point> otherCollisionPoints = other.collisionPoints();

        for (Point point : otherCollisionPoints) {
            if (myCollisionPoints.contains(point)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Kill the snake and all of its body parts.
     */
    @Override
    public void die() {
        for (BodyPart part : body) {
            part.die();
        }

        super.die();
    }
}
