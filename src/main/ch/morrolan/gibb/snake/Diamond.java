package main.ch.morrolan.gibb.snake;

import java.awt.Point;
import java.awt.Color;
import java.awt.Font;

public class Diamond extends Entity
{
    public Diamond() {
        super();
    }

    public Diamond(Point point) {
        super(point);
    }

    public Diamond(int x, int y) {
        super(x, y);
    }


    @Override
    public void draw(Painter painter) {
        double[] x = new double[] {position.x + 0.5, position.x + 1, position.x + 0.5, position.x};
        double[] y = new double[] {position.y, position.y + 0.5, position.y + 1, position.y + 0.5};

        painter.fillPolygon(x, y);

        painter.graphics.setColor(new Color(255, 0, 0));
//
        painter.drawLine(x[0], y[0], x[1], y[1]);
        painter.drawLine(x[1], y[1], x[2], y[2]);
        painter.drawLine(x[2], y[2], x[3], y[3]);
        painter.drawLine(x[3], y[3], x[0], y[0]);

        painter.graphics.setColor(new Color(0, 0, 0));

    }


}
