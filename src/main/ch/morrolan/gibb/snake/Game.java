package main.ch.morrolan.gibb.snake;

import java.awt.Image;
import java.awt.Point;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;


public class Game implements KeyListener
{
    private final int X_GRID_COUNT = 20;
    private final int Y_GRID_COUNT = 20;
    private final int X_TILE_WIDTH = 30;
    private final int Y_TILE_WIDTH = 30;

    private final int xGridSize = X_GRID_COUNT * X_TILE_WIDTH;
    private final int yGridSize = Y_GRID_COUNT * Y_TILE_WIDTH;


    private final int DIAMOND_COUNT = 20;

    public GUI gui;
    public Painter painter;
    public Image image;
    public List<Entity> entities;

    public boolean IMALIVE;
    public Snake snake;
    public GameGrid grid;

    public Game() {
        gui = new GUI(xGridSize, yGridSize);
        gui.setVisible(true);
        gui.addKeyListener(this);

        image = gui.createImage(gui.getWidth(), gui.getHeight());
        painter = new Painter(image.getGraphics(), X_TILE_WIDTH, Y_TILE_WIDTH);

        entities = new ArrayList<Entity>();

    }

    public void setUpEntities() {
        entities.clear();

        grid = new GameGrid(X_GRID_COUNT, Y_GRID_COUNT);
        entities.add(grid);

        setUpDiamonds(DIAMOND_COUNT);

        snake = new Snake(1, 1);
        System.out.println("snake.position = " + snake.position);
        entities.add(snake);
    }

    public void setUpDiamonds(int count) {
        List<Point> points = new ArrayList<Point>();

        for (int i = 0; i < X_GRID_COUNT; i++) {
            for (int j = 0; j < Y_GRID_COUNT; j++) {
                points.add(new Point(i, j));
            }
        }
        Collections.shuffle(points);

        if (count > X_GRID_COUNT * Y_GRID_COUNT) {
            count = X_GRID_COUNT * Y_GRID_COUNT;
        }

        for (int i = 0; i < count; i++) {
            entities.add(new Diamond(points.get(i)));
        }
    }

    public void startGame() {
        IMALIVE = true;
        int tickCount = 0;

        setUpEntities();

        while (IMALIVE) {
            tick(tickCount);
            tickCount++;

            collide();

            burnTheDead();

            draw(painter);

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        painter.fillRect(0, 0, grid.width, grid.height);
        gui.drawImage(image);
        System.out.println("Game over. :(");
    }

    public void burnTheDead() {
        Iterator<Entity> it = entities.iterator();
        while (it.hasNext()) {
            Entity nextEntity = it.next();
            if (!nextEntity.alive) {
                it.remove();
            }
        }
    }

    public void draw(Painter painter) {
        painter.graphics.clearRect(0, 0, gui.getWidth(), gui.getHeight());

        for (Entity entity : entities) {
            entity.draw(painter);
        }

        gui.drawImage(image);
    }

    public void collide() {
        // Todo: Don't quite it this way, as-is only the snake gets to check for collisions, what if we had other 'interactive' objects?
        for (Entity entity : entities) {
            if (entity.collidesWith(snake)) {
                if (entity.getClass().equals(Snake.class)) {
                    List<Point> snakePositions = snake.positions();
                    for (Point point : snakePositions) {
                        if (Collections.frequency(snakePositions, point) > 1) {
                            snake.die();
                        }
                    }
                }
                else if (entity.getClass().equals(Diamond.class)) {
                    entity.die();
                    snake.growBodyPart();
                }
                else if (entity.getClass().equals((GameGrid.class))) {
                    // Todo: Implement once GameGrid has a working collidesWith() function.
                }
            }
        }

        // Todo: Move this up once ready.
        BodyPart part = snake.body.get(0);
        if (part.position.x < grid.position.x || part.position.x + 1 > grid.endPoint().x || part.position.y < grid.position.y || part.position.y + 1 > grid.endPoint().y) {
            snake.die();
        }


        IMALIVE = snake.alive;
    }

    public void tick(long tickCount) {
        for (Entity entity : entities) {
            entity.tick(tickCount);
        }
    }

    /**
     * Invoked when a key has been typed.
     * See the class description for {@link java.awt.event.KeyEvent} for a definition of
     * a key typed event.
     */
    @Override
    public void keyTyped(KeyEvent e) {
    }

    /**
     * Invoked when a key has been pressed.
     * See the class description for {@link java.awt.event.KeyEvent} for a definition of
     * a key pressed event.
     */
    @Override
    public void keyPressed(KeyEvent e) {
        System.out.println("e.getKeyCode() = " + e.getKeyCode());
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP:
                // Arrow up
                snake.direction = Direction.UP;
                break;
            case KeyEvent.VK_RIGHT:
                // Arrow right
                snake.direction = Direction.RIGHT;
                break;
            case KeyEvent.VK_DOWN:
                // Arrow down
                snake.direction = Direction.DOWN;
                break;
            case KeyEvent.VK_LEFT:
                // Arrow left
                snake.direction = Direction.LEFT;
                break;
            case KeyEvent.VK_SPACE:
                // Space
                snake.frozen = !snake.frozen;
                break;
            case KeyEvent.VK_Q:
                // q
                snake.growBodyPart();
                break;
            case KeyEvent.VK_R:
                //r
                setUpEntities();
                break;
            case KeyEvent.VK_W:
                // w
                snake.growHead();
                break;
            case KeyEvent.VK_I:
                // i
                System.out.println("snake.positions() = " + snake.positions());
        }
    }

    /**
     * Invoked when a key has been released.
     * See the class description for {@link java.awt.event.KeyEvent} for a definition of
     * a key released event.
     */
    @Override
    public void keyReleased(KeyEvent e) {
    }
}