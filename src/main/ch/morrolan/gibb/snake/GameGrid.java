package main.ch.morrolan.gibb.snake;

import java.awt.Point;

import java.util.List;
import java.util.ArrayList;

public class GameGrid extends Entity
{
    public int width;
    public int height;

    public GameGrid(int width, int height) {
        super();

        this.width = width;
        this.height = height;
    }

    public GameGrid(Point position, int width, int height) {
        super(position);

        this.width = width;
        this.height = height;
    }

    public GameGrid(int x, int y, int width, int height) {
        super(x, y);

        this.width = width;
        this.height = height;
    }


    @Override
    public void draw(Painter painter) {
        // The border around the field.
        painter.drawRect(position.x, position.y, width, height);

        // Horizontal lines.
        for (int i = position.y; i <= endPoint().y; i++) {
            painter.drawLine(position.x, i, endPoint().x, i);
        }

        // Vertical lines.
        for (int i = position.x; i <= endPoint().x; i++) {
            painter.drawLine(i, position.y, i, endPoint().y);
        }
    }


    /**
     * Return the second point of the game grid, calculated from its top left corner and its width and height.
     * @return Bottom-right point of game grid.
     */
    public Point endPoint() {
        return new Point(position.x + width, position.y + height);
    }
}
