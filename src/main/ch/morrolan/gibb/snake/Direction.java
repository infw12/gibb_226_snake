package main.ch.morrolan.gibb.snake;

/**
 * An enumeration, representing the four directions (Up, Down, Left, Right) an entity can face on a two-dimensional plane.
 * Merely moving entities (BodyPart -> Head & Snake) have a direction.
 */
public enum Direction
{
    UP,
    DOWN,
    LEFT,
    RIGHT
}
