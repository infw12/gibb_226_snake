package main.ch.morrolan.gibb.snake;

import java.awt.Point;

/**
 * A class which represents a single body part of a snake.
 */
public class BodyPart extends Entity
{
    public Direction direction;

    /**
     * Create a new body part, defaulting to the entity's default position.
     */
    public BodyPart() {
        super();
        direction = Direction.DOWN;
    }

    /**
     * Create a new body part, using the supplied point as initial position.
     * @param point The point which to use as initial position.
     */
    public BodyPart(Point point) {
        super(point);
        direction = Direction.DOWN;
    }

    /**
     * Create a new body part, using the supplied coordinates as initial position.
     * @param x X value which to use as initial position.
     * @param y Y value which to use as initial position.
     */
    public BodyPart(int x, int y) {
        super(x, y);
        direction = Direction.DOWN;
    }

    /**
     * Move the body part in the appropriate direction.
     * @param amount The number of tiles which to move the body part.
     */
    public void move(int amount) {
        switch (direction) {
            case UP:
                moveUp(amount);
                break;
            case DOWN:
                moveDown(amount);
                break;
            case LEFT:
                moveLeft(amount);
                break;
            case RIGHT:
                moveRight(amount);
                break;
        }
    }

    @Override
    public void draw(Painter painter) {
        painter.fillOval(position.x, position.y, 1, 1);
    }
}