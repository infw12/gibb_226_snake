package main.ch.morrolan.gibb.snake;

import java.awt.Graphics;

/**
 * A utility class, which handles the drawing of objects the size of which is specified as grid tiles on a graphics object.
 * It provides a subset of the functions with the same signatures which the Graphics class does provide.
 */
public class Painter
{
    public Graphics graphics;
    public int tileSizeX;
    public int tileSizeY;

    /**
     * Create a new painter, painting on a grid consisting of cuboid tiles.
     * @param g The graphics on which to draw.
     * @param tileSize The width/height of one tile in pixels.
     */
    public Painter(Graphics g, int tileSize) {
        graphics = g;
        setTileSize(tileSize);
    }

    /**
     * Create a new painter, painting on a grid consisting of rectangular tiles.
     * @param g The graphics on which to draw.
     * @param tileSizeX The width of one tile in pixels.
     * @param tileSizeY The height of one tile in pixels.
     */
    public Painter(Graphics g, int tileSizeX, int tileSizeY) {
        graphics = g;
        this.tileSizeX = tileSizeX;
        this.tileSizeY = tileSizeY;
    }

    /**
     * Sets the width and height of one tile.
     * @param tileSize The width/height of one tile in pixels.
     */
    public void setTileSize(int tileSize) {
        tileSizeX = tileSize;
        tileSizeY = tileSize;
    }

    /**
     * Draws a rectangle on the graphics.
     * @param x X coordinate of top left corner of the rectangle, as grid coordinate.
     * @param y Y coordinate of top left corner of the rectangle, as grid coordinate.
     * @param width Width of the rectangle, in grid tiles.
     * @param height Height of the rectangle, in grid tiles.
     */
    public void drawRect(double x, double y, double width, double height) {
        int _x = adjustX(x);
        int _y = adjustY(y);
        int _width = adjustX(width);
        int _height = adjustY(height);

        graphics.drawRect(_x, _y, _width, _height);
    }

    /**
     * Fills a rectangle on the graphics.
     * @param x X coordinate of the top left corner of the rectangle, as grid coordinate.
     * @param y Y coordinate of the top left corner of the rectangle, as grid coordinate.
     * @param width Width of the rectangle, in grid tiles.
     * @param height Height of the rectangle, in grid tiles.
     */
    public void fillRect(double x, double y, double width, double height) {
        int _x = adjustX(x);
        int _y = adjustY(y);
        int _width = adjustX(width);
        int _height = adjustY(height);

        graphics.fillRect(_x, _y, _width, _height);
    }

    /**
     * Draws an oval on the graphics.
     * @param x X coordinate of the top left corner of the oval, as grid coordinate.
     * @param y Y coordinate of the top left corner of the oval, as grid coordinate.
     * @param width Width of the oval, in grid tiles.
     * @param height Height of the oval, in grid tiles.
     */
    public void drawOval(double x, double y, double width, double height) {
        int _x = adjustX(x);
        int _y = adjustY(y);
        int _width = adjustX(width);
        int _height = adjustY(height);

        graphics.drawOval(_x, _y, _width, _height);
    }

    /**
     * Fills an oval on the graphics.
     * @param x X coordinate of the top left corner of the oval, as grid coordinate.
     * @param y Y coordinate of the top left corner of the oval, as grid coordinate.
     * @param width Width of the oval, in grid tiles.
     * @param height Height of the oval, in grid tiles.
     */
    public void fillOval(double x, double y, double width, double height) {
        int _x = adjustX(x);
        int _y = adjustY(y);
        int _width = adjustX(width);
        int _height = adjustY(height);

        graphics.fillOval(_x, _y, _width, _height);
    }

    /**
     * Draws a line on the graphics.
     * @param x1 X coordinate of the first point of the line, as grid coordinate.
     * @param y1 Y coordinate of the first point of the line, as grid coordinate.
     * @param x2 X coordinate of the second point of the line, as grid coordinate.
     * @param y2 Y coordinate of the second point of the line, as grid coordinate.
     */
    public void drawLine(double x1, double y1, double x2, double y2) {
        int _x1 = adjustX(x1);
        int _y1 = adjustY(y1);
        int _x2 = adjustX(x2);
        int _y2 = adjustY(y2);

        graphics.drawLine(_x1, _y1, _x2, _y2);
    }

    /**
     * Draws a polygon on the graphics.
     * @param x Array of X values of the points.
     * @param y Array of Y values of the points.
     */
    public void fillPolygon(double[] x, double[] y) {
        // Todo: Tests
        int[] _x = new int[x.length];
        int[] _y = new int[y.length];

        for (int i = 0; i < x.length; i++) {
            _x[i] = adjustX(x[i]);
        }

        for (int i = 0; i < y.length; i++) {
            _y[i] = adjustY(y[i]);
        }

        int len = x.length <= y.length ? x.length : y.length;

        graphics.fillPolygon(_x, _y, len);
    }

    /**
     * Convert grid X-coordinates to pixels.
     * @param x X coordinate, grid.
     * @return X coordinate, pixels.
     */
    private int adjustX(double x) {
        return (int)Math.round(x * tileSizeX);
    }

    /**
     * Convert grid Y-coordinate to pixels.
     * @param y Y coordinate, grid.
     * @return Y coordinate, pixels.
     */
    private int adjustY(double y) {
        return (int)Math.round(y * tileSizeY);
    }
}
