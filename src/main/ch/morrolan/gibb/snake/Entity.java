package main.ch.morrolan.gibb.snake;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/**
 * A class which represents any entity which can be part of the game. A snake, a part of the snake's body, a diamond, the game border etc.
 */
public class Entity
{
    public Point position;
    public boolean alive;

    /**
     * Create a new entity, defaulting to (0, 0) as its position.
     */
    public Entity() {
        position = new Point(0, 0);
        alive = true;

    }

    /**
     * Create a new entity, using the supplied argument as its position.
     * @param point Initial position of the entity.
     */
    public Entity(Point point) {
        position = point;
        alive = true;
    }

    /**
     * Create a new entity, using the supplied arguments as the coordinates of its position.
     * @param x The initial position's X coordinate.
     * @param y The initial position's Y coordinate.
     */
    public Entity(int x, int y) {
        position = new Point(x, y);
        alive = true;
    }


    /**
     * 'Kills' the entity, and makes it clean up after itself. If subclasses need to clean anything up, they should override this functionality.
     */
    public void die() {
        alive = false;
    }


    /**
     * Move the entity along the X axis.
     * @param i The number of tiles which to move the entity.
     */
    public void moveX(int i) {
        position.x += i;
    }

    /**
     * Move the entity along the Y axis.
     * @param i The number of tiles which to move the entity.
     */
    public void moveY(int i) {
        position.y += i;
    }


    /**
     * Move the entity upwards (-Y).
     * @param i The number of tiles which to move the entity.
     */
    public void moveUp(int i) {
        moveY(-i);
    }

    /**
     * Move the entity downwards (+Y).
     * @param i The number of tiles which to move the entity.
     */
    public void moveDown(int i) {
        moveY(i);
    }

    /**
     * Move the entity to the left (-X).
     * @param i The number of tiles which to move the entity.
     */
    public void moveLeft(int i) {
        moveX(-i);
    }

    /**
     * Move the entity to the right (+X).
     * @param i The number of tiles which to move the entity.
     */
    public void moveRight(int i) {
        moveX(i);
    }


    /**
     * Make the entity perform logic operations. Should be overwritten by subclasses which need to perform any logic.
     * @param tickCount The number of ticks which have passed since the beginning of the game.
     */
    public void tick(long tickCount) {
        // Placeholder
    }

    /**
     * Make the entity draw itself using the Painter utility class. Should be overwritten by subclasses which need to draw something.
     * @param painter An instance of the utility class which takes care of the transformation between grid tiles to pixels.
     */
    public void draw(Painter painter) {
        // Placeholder
    }

    // Todo: Tests for these two functions.
    public List<Point> collisionPoints() {
        List<Point> collisionPoints = new ArrayList<Point>(1);
        collisionPoints.add(position);
        return collisionPoints;
    }

    public boolean collidesWith(Entity other) {
        return other.collisionPoints().contains(position);
    }
}