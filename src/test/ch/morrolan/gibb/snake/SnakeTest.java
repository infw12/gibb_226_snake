package test.ch.morrolan.gibb.snake;

import java.awt.Point;
import java.awt.Graphics;
import java.awt.Color;

import java.util.List;
import java.util.ArrayList;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Ignore;
import org.junit.runners.JUnit4;

import main.ch.morrolan.gibb.snake.*;

@RunWith(JUnit4.class)
public class SnakeTest {
    public Snake longSnake() {
        Snake snake = new Snake(0, 3);
        snake.body.add(new BodyPart(0, 2));
        snake.body.add(new BodyPart(0, 1));
        snake.body.add(new BodyPart(0, 0));
        /*
            0
        0   B
        1   B
        2   B
        3   H
         */

        return snake;
    }

    @Test
    public void Snake() {
        Snake snake = new Snake();
        assertEquals("Eve", snake.name);
        assertEquals(new Point(0, 0), snake.body.get(0).position);
        assertEquals(Direction.DOWN, snake.direction);
    }

    @Test
    public void positions() {
        Snake snake = longSnake();
        List<Point> positions = new ArrayList<Point>();
        for (BodyPart part : snake.body) {
            positions.add(part.position);
        }

        assertEquals(positions, snake.positions());
    }

    @Test
    public void moveX() {
        Snake snake = new Snake();

        snake.moveX(5);
        assertEquals(new Point(5, 0), snake.body.get(0).position);

        snake.moveX(-12);
        assertEquals(new Point(-7, 0), snake.body.get(0).position);
    }

    @Test
    public void moveY() {
        Snake snake = new Snake();

        snake.moveY(10);
        assertEquals(new Point(0, 10), snake.body.get(0).position);

        snake.moveY(-16);
        assertEquals(new Point(0, -6), snake.body.get(0).position);
    }

    @Test
    public void moveUp() {
        Snake snake = new Snake();

        snake.moveUp(7);
        assertEquals(new Point(0, -7), snake.body.get(0).position);

        snake.moveUp(-9);
        assertEquals(new Point(0, 2), snake.body.get(0).position);
    }

    @Test
    public void moveDown() {
        Snake snake = new Snake();

        snake.moveDown(3);
        assertEquals(new Point(0, 3), snake.body.get(0).position);

        snake.moveDown(-5);
        assertEquals(new Point(0, -2), snake.body.get(0).position);
    }

    @Test
    public void moveLeft() {
        Snake snake = new Snake();

        snake.moveLeft(11);
        assertEquals(new Point(-11, 0), snake.body.get(0).position);

        snake.moveLeft(-14);
        assertEquals(new Point(3, 0), snake.body.get(0).position);
    }

    @Test
    public void moveRight() {
        Snake snake = new Snake();

        snake.moveRight(2);
        assertEquals(new Point(2, 0), snake.body.get(0).position);

        snake.moveRight(-3);
        assertEquals(new Point(-1, 0), snake.body.get(0).position);
    }

    @Test
    public void tick() {
        Snake snake = new Snake();

        Head head = mock(Head.class);
        BodyPart part = mock(BodyPart.class);
        snake.body.add(head);
        snake.body.add(part);
        snake.tick(4);
        verify(head).tick(4);
        verify(part).tick(4);

        snake.tick(1);
        assertEquals(new Point(0, 0), snake.body.get(0).position);
        snake.tick(4);
        assertEquals(new Point(0, 0), snake.body.get(0).position);

        snake.tick(5);
        assertEquals(new Point(0, 1), snake.body.get(0).position);
        snake.tick(9);
        assertEquals(new Point(0, 1), snake.body.get(0).position);

        snake.tick(10);
        assertEquals(new Point(0, 2), snake.body.get(0).position);

        snake.direction = Direction.LEFT;
        snake.tick(15);
        assertEquals(new Point(-1, 2), snake.body.get(0).position);

        snake.direction = Direction.RIGHT;
        snake.tick(20);
        assertEquals(new Point(0, 2), snake.body.get(0).position);

        snake.direction = Direction.UP;
        snake.tick(25);
        assertEquals(new Point(0, 1), snake.body.get(0).position);

        snake.direction = Direction.DOWN;
        snake.tick(30);
        assertEquals(new Point(0, 2), snake.body.get(0).position);
    }

    // Todo: Tests for all four directions.
    @Test
    public void growHead() {
        Snake snake = new Snake(4, 3);
        snake.direction = Direction.DOWN;

        // Growing a head should add a head at the end of the snake.
        snake.growHead();
        assertEquals(2, snake.body.size());
        assertEquals(Head.class, snake.body.get(1).getClass());
        assertEquals(new Point(4, 2), snake.body.get(1).position);

        // Growing another head should add it according to the direction of the snake's LAST element, and not according to the snake's FIRST element.
        // Note: We'll set the direction of the first part directly. Normally this'd be done by having the snake move, the first part would then get the direction of the snake itself assigned.
        snake.body.get(0).direction = Direction.LEFT;
        snake.growHead();
        assertEquals(3, snake.body.size());
        assertEquals(Head.class, snake.body.get(2).getClass());
        assertEquals(new Point(4, 1), snake.body.get(2).position);

        // In this case the new head should get added to the right of the last element. (Since the last element is moving left.)
        snake.body.get(2).direction = Direction.LEFT;
        snake.growHead();
        assertEquals(4, snake.body.size());
        assertEquals(Head.class, snake.body.get(3).getClass());
        assertEquals(new Point(5, 1), snake.body.get(3).position);

    }

    @Test
    public void growBodyPart() {
        Snake snake = new Snake(4, 3);
        snake.direction = Direction.DOWN;

        // Growing a body part should add a head at the end of the snake.
        snake.growBodyPart();
        assertEquals(2, snake.body.size());
        assertEquals(BodyPart.class, snake.body.get(1).getClass());
        assertEquals(new Point(4, 2), snake.body.get(1).position);

        // Growing another body part should add it according to the direction of the snake's LAST element, and not according to the snake's FIRST element.
        // Note: We'll set the direction of the first part directly. Normally this'd be done by having the snake move, the first part would then get the direction of the snake itself assigned.
        snake.body.get(0).direction = Direction.LEFT;
        snake.growBodyPart();
        assertEquals(3, snake.body.size());
        assertEquals(BodyPart.class, snake.body.get(2).getClass());
        assertEquals(new Point(4, 1), snake.body.get(2).position);

        // In this case the new body part should get added to the right of the last element. (Since the last element is moving left.)
        snake.body.get(2).direction = Direction.LEFT;
        snake.growBodyPart();
        assertEquals(4, snake.body.size());
        assertEquals(BodyPart.class, snake.body.get(3).getClass());
        assertEquals(new Point(5, 1), snake.body.get(3).position);
    }

    @Test
    public void draw() {
        Snake snake = new Snake();

        Head head = mock(Head.class);
        BodyPart part = mock(BodyPart.class);
        Painter painter = mock(Painter.class);
        painter.graphics = mock(Graphics.class);

        snake.body.add(head);
        snake.body.add(part);
        snake.draw(painter);
        verify(head).draw(painter);
        verify(part).draw(painter);
    }

    @Test
    public void moveLongSnake() {
        Snake snake = longSnake();

        // Moving all of the snake down.
        /*
            0
        0
        1   B
        2   B
        3   B
        4   H
         */

        snake.tick(5);
        for (int i = 0; i < snake.body.size(); i++) {
            assert(snake.positions().contains(new Point(0, i * 1 + 1)));
        }

        snake.direction = Direction.RIGHT;
        // Now its head should go right, while the rest of the body continues downwards.
        /*
            0 1
        0
        1
        2   B
        3   B
        4   B H
         */
        snake.tick(5);
        assertEquals(new Point(1, 4), snake.body.get(0).position);
        assertEquals(new Point(0, 4), snake.body.get(1).position);
        assertEquals(new Point(0, 3), snake.body.get(2).position);
        assertEquals(new Point(0, 2), snake.body.get(3).position);

        snake.direction = Direction.UP;
        // Its head should go up, the first bodypart right, and the other two bodyparts down.
        /*
            0 1
        0
        1
        2
        3   B H
        4   B B
         */
        snake.tick(5);
        assertEquals(new Point(1, 3), snake.body.get(0).position);
        assertEquals(new Point(1, 4), snake.body.get(1).position);
        assertEquals(new Point(0, 4), snake.body.get(2).position);
        assertEquals(new Point(0, 3), snake.body.get(3).position);

        // One more tick.
         /*
            0 1
        0
        1
        2     H
        3     B
        4   B B
         */
        snake.tick(5);
        assertEquals(new Point(1, 2), snake.body.get(0).position);
        assertEquals(new Point(1, 3), snake.body.get(1).position);
        assertEquals(new Point(1, 4), snake.body.get(2).position);
        assertEquals(new Point(0, 4), snake.body.get(3).position);

        snake.direction = Direction.LEFT;
        // Its head should go left, the first two body parts up, and the last bodypart right.
         /*
            0 1
        0
        1
        2   H B
        3     B
        4     B
         */
        snake.tick(5);
        assertEquals(new Point(0, 2), snake.body.get(0).position);
        assertEquals(new Point(1, 2), snake.body.get(1).position);
        assertEquals(new Point(1, 3), snake.body.get(2).position);
        assertEquals(new Point(1, 4), snake.body.get(3).position);
    }

    @Test
    public void die() {
        Snake snake = longSnake();
        snake.die();

        for (Entity entity : snake.body) {
            assertEquals(false, entity.alive);
        }

        assertEquals(false, snake.alive);
    }


}
