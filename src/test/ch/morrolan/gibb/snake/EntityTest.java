package test.ch.morrolan.gibb.snake;

import java.awt.Point;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import main.ch.morrolan.gibb.snake.Entity;

@RunWith(JUnit4.class)
public class EntityTest {
    // Todo: A before hook would cut down on the boilerplate. (Initializing entity = new Entity(0, 0))
    // Todo: If JUnit provides an "expect bla to change x by z" we could write the move... tests a tad cleaner.

    @Test
    public void Entity() {
        Entity entity;

        entity = new Entity();
        assertEquals(new Point(0, 0), entity.position);
        assertEquals(true, entity.alive);

        entity = new Entity(7, 11);
        assertEquals(new Point(7, 11), entity.position);
        assertEquals(true, entity.alive);

        entity = new Entity(new Point(11, 2));
        assertEquals(new Point(11, 2), entity.position);
        assertEquals(true, entity.alive);
    }

    @Test
    public void moveX() {
        Entity entity = new Entity(0, 0);

        entity.moveX(7);
        assertEquals(new Point(7, 0), entity.position);

        entity.moveX(-11);
        assertEquals(new Point(-4, 0), entity.position);
    }

    @Test
    public void moveY() {
        Entity entity = new Entity(0, 0);

        entity.moveY(-21);
        assertEquals(new Point(0, -21), entity.position);

        entity.moveY(25);
        assertEquals(new Point(0, 4), entity.position);
    }

    @Test
    public void moveUp() {
        Entity entity = new Entity(0, 0);

        entity.moveUp(5);
        assertEquals(new Point(0, -5), entity.position);

        entity.moveUp(-7);
        assertEquals(new Point(0, 2), entity.position);
    }

    @Test
    public void moveDown() {
        Entity entity = new Entity(0, 0);

        entity.moveDown(11);
        assertEquals(new Point(0, 11), entity.position);

        entity.moveDown(-15);
        assertEquals(new Point(0, -4), entity.position);
    }

    @Test
    public void moveLeft() {
        Entity entity = new Entity(0, 0);

        entity.moveLeft(51);
        assertEquals(new Point(-51, 0), entity.position);

        entity.moveLeft(-70);
        assertEquals(new Point(19, 0), entity.position);
    }

    @Test
    public void moveRight() {
        Entity entity = new Entity(0, 0);

        entity.moveRight(40);
        assertEquals(new Point(40, 0), entity.position);

        entity.moveRight(-61);
        assertEquals(new Point(-21, 0), entity.position);
    }

    @Test
    public void tick() {
        // Placeholder
    }

    @Test
    public void draw() {
        // Placeholder
    }

    @Test
    public void die() {
        Entity entity = new Entity(0, 0);
        entity.die();
        assertEquals(false, entity.alive);
    }
}
