package test.ch.morrolan.gibb.snake;

import java.awt.Point;
import java.awt.Graphics;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

import main.ch.morrolan.gibb.snake.Direction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import main.ch.morrolan.gibb.snake.BodyPart;
import main.ch.morrolan.gibb.snake.Painter;

@RunWith(JUnit4.class)
public class BodyPartTest {
    public BodyPart bodyPart1() {
        return new BodyPart(0, 0);
    }

    public BodyPart bodyPart2() {
        return new BodyPart(7, 2);
    }

    public Painter painterMock() {
        return mock(Painter.class);
    }

    @Test
    public void BodyPart() {
        BodyPart part = new BodyPart();
        assertEquals(Direction.DOWN, part.direction);
    }

    @Test
    public void move() {
        BodyPart part = new BodyPart(0, 0);
        Point position = new Point(0, 0);

        part.direction = Direction.DOWN;
        part.move(1);
        position.y += 1;
        assertEquals(position, part.position);

        part.direction = Direction.RIGHT;
        part.move(5);
        position.x += 5;
        assertEquals(position, part.position);

        part.direction = Direction.UP;
        part.move(3);
        position.y -= 3;
        assertEquals(position, part.position);

        part.direction = Direction.LEFT;
        part.move(2);
        position.x -= 2;
        assertEquals(position, part.position);
    }

    @Test
    public void draw() {
        BodyPart bodyPart;
        Painter painter;

        bodyPart = bodyPart1();
        painter = painterMock();
        bodyPart.draw(painter);
        verify(painter).fillOval(0, 0, 1, 1);

        bodyPart = bodyPart2();
        painter = painterMock();
        bodyPart.draw(painter);
        verify(painter).fillOval(7, 2, 1, 1);

    }

}
