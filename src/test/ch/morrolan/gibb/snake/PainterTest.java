package test.ch.morrolan.gibb.snake;

import java.awt.Graphics;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;


import main.ch.morrolan.gibb.snake.Painter;

@RunWith(JUnit4.class)
public class PainterTest
{
    // Todo: Tests for draw...() with non-even positions / widths / heights.
    public Graphics gMock() {
        return mock(Graphics.class);
    }

    public Painter painter1(Graphics g) {
        return new Painter(g, 10);
    }

    public Painter painter2(Graphics g) {
        return new Painter(g, 7, 9);
    }

    @Test
    public void Painter() {
        Graphics g;
        Painter painter;

        g = gMock();
        painter = painter1(g);
        assertEquals(g, painter.graphics);
        assertEquals(10, painter.tileSizeX);
        assertEquals(10, painter.tileSizeY);

        g = gMock();
        painter = painter2(g);
        assertEquals(g, painter.graphics);
        assertEquals(7, painter.tileSizeX);
        assertEquals(9, painter.tileSizeY);
    }

    @Test
    public void setTileSize() {
        Painter painter;
        Graphics g = gMock();

        painter = painter2(g);
        painter.setTileSize(7);
        assertEquals(7, painter.tileSizeX);
        assertEquals(7, painter.tileSizeY);
    }

    @Test
    public void drawRect() {
        Graphics g;
        Painter painter;

        g = gMock();
        painter = painter1(g);

        painter.drawRect(0, 0, 1, 1);
        verify(g).drawRect(0, 0, 10, 10);

        painter.drawRect(3, 5, 2, 3);
        verify(g).drawRect(30, 50, 20, 30);

        painter.setTileSize(5);
        painter.drawRect(6, 1, 6, 5);
        verify(g).drawRect(30, 5, 30, 25);

        painter.drawRect(2.5, 1.5, 0.5, 0.5);
        verify(g).drawRect(13, 8, 3, 3);

        g = gMock();
        painter = painter2(g);

        painter.drawRect(0, 0, 1, 1);
        verify(g).drawRect(0, 0, 7, 9);

        painter.drawRect(3, 5, 2, 3);
        verify(g).drawRect(21, 45, 14, 27);

        painter.drawRect(3.5, 4.5, 6, 6.5);
        verify(g).drawRect(25, 41, 42, 59);

        painter.setTileSize(5);
        painter.drawRect(6, 1, 6, 5);
        verify(g).drawRect(30, 5, 30, 25);
    }

    @Test
    public void fillRect() {
        Graphics g;
        Painter painter;

        g = gMock();
        painter = painter1(g);

        painter.fillRect(0, 0, 1, 1);
        verify(g).fillRect(0, 0, 10, 10);

        painter.fillRect(3, 5, 2, 3);
        verify(g).fillRect(30, 50, 20, 30);

        painter.setTileSize(5);
        painter.fillRect(6, 1, 6, 5);
        verify(g).fillRect(30, 5, 30, 25);


        g = gMock();
        painter = painter2(g);

        painter.fillRect(0, 0, 1, 1);
        verify(g).fillRect(0, 0, 7, 9);

        painter.fillRect(3, 5, 2, 3);
        verify(g).fillRect(21, 45, 14, 27);

        painter.setTileSize(5);
        painter.fillRect(6, 1, 6, 5);
        verify(g).fillRect(30, 5, 30, 25);

    }

    @Test
    public void drawOval() {
        Graphics g;
        Painter painter;

        g = gMock();
        painter = painter1(g);

        painter.drawOval(0, 0, 1, 1);
        verify(g).drawOval(0, 0, 10, 10);

        painter.drawOval(3, 5, 2, 3);
        verify(g).drawOval(30, 50, 20, 30);

        painter.setTileSize(5);
        painter.drawOval(6, 1, 6, 5);
        verify(g).drawOval(30, 5, 30, 25);

        g = gMock();
        painter = painter2(g);

        painter.drawOval(0, 0, 1, 1);
        verify(g).drawOval(0, 0, 7, 9);

        painter.drawOval(3, 5, 2, 3);
        verify(g).drawOval(21, 45, 14, 27);

        painter.setTileSize(5);
        painter.drawOval(6, 1, 6, 5);
        verify(g).drawOval(30, 5, 30, 25);
    }

    @Test
    public void fillOval() {
        Graphics g;
        Painter painter;

        g = gMock();
        painter = painter1(g);

        painter.fillOval(0, 0, 1, 1);
        verify(g).fillOval(0, 0, 10, 10);

        painter.fillOval(3, 5, 2, 3);
        verify(g).fillOval(30, 50, 20, 30);

        painter.setTileSize(5);
        painter.fillOval(6, 1, 6, 5);
        verify(g).fillOval(30, 5, 30, 25);

        g = gMock();
        painter = painter2(g);

        painter.fillOval(0, 0, 1, 1);
        verify(g).fillOval(0, 0, 7, 9);

        painter.fillOval(3, 5, 2, 3);
        verify(g).fillOval(21, 45, 14, 27);

        painter.setTileSize(5);
        painter.fillOval(6, 1, 6, 5);
        verify(g).fillOval(30, 5, 30, 25);
    }

    @Test
    public void drawLine() {
        Graphics g;
        Painter painter;

        g = gMock();
        painter = painter1(g);

        painter.drawLine(0, 0, 1, 1);
        verify(g).drawLine(0, 0, 10, 10);

        painter.drawLine(3, 5, 2, 3);
        verify(g).drawLine(30, 50, 20, 30);

        painter.setTileSize(5);
        painter.drawLine(6, 1, 6, 5);
        verify(g).drawLine(30, 5, 30, 25);


        g = gMock();
        painter = painter2(g);

        painter.drawLine(0, 0, 1, 1);
        verify(g).drawLine(0, 0, 7, 9);

        painter.drawLine(3, 5, 2, 3);
        verify(g).drawLine(21, 45, 14, 27);

        painter.setTileSize(5);
        painter.drawLine(6, 1, 6, 5);
        verify(g).drawLine(30, 5, 30, 25);
    }

    @Test
    public void fillPolygon() {
        Graphics g;
        Painter painter;

        double[] xCoordinates = {0.2, 4, 5.5, 1};
        double[] yCoordinates = {6.5, 1.2, 3.6, 4.5};

        g = gMock();
        painter = painter1(g);
        painter.fillPolygon(xCoordinates, yCoordinates);
        verify(g).fillPolygon(new int[] {2, 40, 55, 10}, new int[] {65, 12, 36, 45}, 4);

        g = gMock();
        painter = painter2(g);
        painter.fillPolygon(xCoordinates, yCoordinates);
        verify(g).fillPolygon(new int[] {1, 28, 39, 7}, new int[] {59, 11, 32, 41}, 4);
    }
}
