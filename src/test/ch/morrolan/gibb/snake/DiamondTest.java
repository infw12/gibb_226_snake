package test.ch.morrolan.gibb.snake;

import java.awt.Point;
import java.awt.Graphics;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

import main.ch.morrolan.gibb.snake.Direction;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import main.ch.morrolan.gibb.snake.Diamond;
import main.ch.morrolan.gibb.snake.Painter;

@RunWith(JUnit4.class)
public class DiamondTest {
    public Diamond diamond1() {
        return new Diamond(0, 0);
    }

    public Diamond diamond2() {
        return new Diamond(7, 2);
    }

    public Painter painterMock() {
        return mock(Painter.class);
    }

    public Graphics gMock() {
        return mock(Graphics.class);
    }


    @Test
    public void draw() {
        Diamond bodyPart;
        Painter painter;

        bodyPart = diamond1();
        painter = painterMock();
        painter.graphics = gMock();
        bodyPart.draw(painter);
        double[] xCoordinates = {0.5, 1, 0.5, 0};
        double[] yCoordinates = {0, 0.5, 1, 0.5};
        verify(painter).fillPolygon(xCoordinates, yCoordinates);

        for (int i = 0; i <= 2; i++) {
            verify(painter).drawLine(xCoordinates[i], yCoordinates[i], xCoordinates[i+1], yCoordinates[i+1]);
        }

        bodyPart = diamond2();
        painter = painterMock();
        painter.graphics = gMock();
        bodyPart.draw(painter);
        xCoordinates = new double[] {7.5, 8, 7.5, 7};
        yCoordinates = new double[] {2, 2.5, 3, 2.5};
        verify(painter).fillPolygon(xCoordinates, yCoordinates);

        for (int i = 0; i <= 2; i++) {
            verify(painter).drawLine(xCoordinates[i], yCoordinates[i], xCoordinates[i+1], yCoordinates[i+1]);
        }

    }

}
