package test.ch.morrolan.gibb.snake;

import java.awt.Point;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Ignore;
import org.junit.runners.JUnit4;

import main.ch.morrolan.gibb.snake.GameGrid;
import main.ch.morrolan.gibb.snake.Painter;

@RunWith(JUnit4.class)
public class GameGridTest
{
    public GameGrid gameGrid1() {
        return new GameGrid(10, 5);
    }

    public GameGrid gameGrid2() {
        return new GameGrid(3, 5, 6, 4);
    }

    @Test
    public void GameGrid() {
        GameGrid gg;

        gg = gameGrid1();
        assertEquals(10, gg.width);
        assertEquals(5, gg.height);

        gg = new GameGrid(2, 5, 7, 3);
        assertEquals(7, gg.width);
        assertEquals(3, gg.height);

        gg = new GameGrid(new Point(1, 2), 4, 6);
        assertEquals(4, gg.width);
        assertEquals(6, gg.height);

        gg = gameGrid2();
        assertEquals(3, gg.position.x);
        assertEquals(5, gg.position.y);
        assertEquals(6, gg.width);
        assertEquals(4, gg.height);
    }

    @Test
    public void draw() {
        Painter painter = mock(Painter.class);
        GameGrid grid;

        grid = new GameGrid(0, 0, 10, 10);
        grid.draw(painter);
        verify(painter).drawRect(0, 0, 10, 10);
        // Horizontal lines
        for (int i = 0; i <= 10; i++) {
            verify(painter).drawLine(0, i,  10, i);
        }
        // Vertical lines
        for (int i = 0; i <= 10; i++) {
            verify(painter).drawLine(i, 0, i, 10);
        }


        grid = new GameGrid(0, 0, 3, 7);
        grid.draw(painter);
        verify(painter).drawRect(0, 0, 3, 7);
        // Horizontal lines
        for (int i = 0; i <= 7; i++) {
            verify(painter, times(1)).drawLine(0, i, 3, i);
        }
        // Vertical lines
        for (int i = 0; i <= 3; i++) {
            verify(painter).drawLine(i, 0, i, 7);
        }


        grid = gameGrid2();
        grid.draw(painter);
        verify(painter).drawRect(3, 5, 6, 4);
        // Horizontal lines
        for (int i = 5; i <= 9; i++) {
            verify(painter, times(1)).drawLine(3, i, 9, i);
        }
        // Vertical lines
        for (int i = 3; i <= 9; i++) {
            verify(painter).drawLine(i, 5, i, 9);
        }
    }

    @Test
    public void endPoint() {
        GameGrid gg;

        gg = gameGrid1();
        assertEquals(new Point(10, 5), gg.endPoint());

        gg = gameGrid2();
        assertEquals(new Point(9, 9), gg.endPoint());
    }

}
