package test.ch.morrolan.gibb.snake;

import java.util.List;
import java.util.ArrayList;

import java.awt.Graphics;

import static org.mockito.Mockito.*;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.Ignore;
import org.junit.runners.JUnit4;

import main.ch.morrolan.gibb.snake.*;

@RunWith(JUnit4.class)
public class GameTest
{
    @Test
    public void Game() {
        Game game = new Game();
        assertEquals(new ArrayList<Entity>(), game.entities);
    }

    @Test
    public void tick() {
        Game game = new Game();
        Entity e1 = mock(Entity.class);
        Diamond e2 = mock(Diamond.class);
        Snake e3 = mock(Snake.class);

        game.entities.add(e1);
        game.entities.add(e2);
        game.entities.add(e3);

        game.tick(1);
        verify(e1).tick(1);
        verify(e2).tick(1);
        verify(e3).tick(1);
    }

    @Test
    public void draw() {
        Game game = new Game();
        Entity e1 = mock(Entity.class);
        Diamond e2 = mock(Diamond.class);
        Snake e3 = mock(Snake.class);
        Painter painter = mock(Painter.class);
        painter.graphics = mock(Graphics.class);
        game.gui = mock(GUI.class);
        
        game.entities.add(e1);
        game.entities.add(e2);
        game.entities.add(e3);

        game.draw(painter);
        verify(e1).draw(painter);
        verify(e2).draw(painter);
        verify(e3).draw(painter);
    }

    @Test
    public void burnTheDead() {
        Game game = new Game();
        Entity e1 = new Entity();
        Diamond e2 = new Diamond();
        Snake e3 = new Snake();

        game.entities.add(e1);
        game.entities.add(e2);
        game.entities.add(e3);

        e1.die();
        e3.die();
        game.burnTheDead();

        assertFalse(game.entities.contains(e1));
        assertFalse(game.entities.contains(e3));
        assert(game.entities.contains(e2));
    }
}
